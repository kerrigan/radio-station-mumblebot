# README #
Mumble based radiostation site and chatbot

### What is this repository for? ###

* Full internet radio station solution(not now)
* What ready:
	2. Chat
	3. Air schedule
	4. html5 player(not for safari)
	5. embedded twitter widget

### Dependencies ###
1. bitbucket.org/kerrigan/gomumbot
1. github.com/dchest/captcha
1. github.com/gorilla/mux
1. github.com/gorilla/websocket
1. github.com/gorilla/session

Install: go get -u bitbucket.org/kerrigan/gomumbot github.com/dchest/captcha github.com/gorilla/mux github.com/gorilla/websocket

Build: make dist

### Other settings ###
1. Now supported only OPUS, put opusthreshold=0 in /etc/mumble-server.ini of your Mumble server
2. Use liquidsoap to make full sound pipeline(background, music, fallback)
3. Use 2 mounts on icecast, one with fallback, other for live air


### liquidsoap ###
1. sudo apt-get install liquidsoap liquidsoap-plugin-vorbis liquidsoap-plugin-samplerate
2. DON'T INSTALL liquidsoap-plugin-opus, it breaks ogg vorbis

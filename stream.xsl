<xsl:stylesheet xmlns:xsl = "http://www.w3.org/1999/XSL/Transform" version = "1.0" >
	<xsl:import href="xml2json.xslt" />
	<xsl:output omit-xml-declaration="yes" method="text" doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd" indent="no" media-type="application/json" encoding="UTF-8" />
	<xsl:strip-space elements="*"/>
	<xsl:variable name="output">true</xsl:variable>
        <xsl:template match = "/icestats">
		<xsl:for-each select="source[@mount='/mpd.ogg']">{"artist": <xsl:call-template name="escape-string"><xsl:with-param name="s" select="artist" /></xsl:call-template>, "title": <xsl:call-template name="escape-string"><xsl:with-param name="s" select="title" /></xsl:call-template>, "listeners": <xsl:value-of select="listeners" />, "max_listeners": <xsl:value-of select="listener_peak" />}
		</xsl:for-each>
        </xsl:template>
</xsl:stylesheet>

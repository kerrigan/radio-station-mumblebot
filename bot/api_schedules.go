package bot

import (
	"net/http"

	"github.com/gorilla/schema"
)

func (p *RadioStation) APIAdminScheduleNewAir(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()

	var decoder = schema.NewDecoder()
	form := new(NewScheduleForm)

	err := decoder.Decode(form, r.PostForm)
	if err != nil {
		//TODO: write error
		toJson(w, struct {
			Status string `json:"status"`
			Error  string `json:"error"`
		}{
			"error",
			err.Error(),
		})
		return
	}
	p.scheduleStorage.AddSchedule(form.Begin, form.End, form.Title, form.Dj)

	toJson(w, struct {
		Status string `json:"status"`
	}{
		"ok",
	})
}

func (p *RadioStation) APIAdminScheduleDeleteAir(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()

	var decoder = schema.NewDecoder()
	form := new(DeleteScheduleForm)

	err := decoder.Decode(form, r.PostForm)

	if err != nil {
		toJson(w, struct {
			Status string `json:"status"`
			Error  string `json:"error"`
		}{
			"error",
			"Wrong form",
		})
		return
	}

	p.scheduleStorage.RemoveSchedule(form.Index)
}

func (p *RadioStation) APIAdminScheduleEditAir(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()

	form := new(EditScheduleForm)
	var decoder = schema.NewDecoder()
	err := decoder.Decode(form, r.PostForm)

	if err != nil || (form.Index < 0 && form.Index >= len(p.scheduleStorage.Schedules)) {
		toJson(w, struct {
			Status string `json:"status"`
			Error  string `json:"error"`
		}{
			"error",
			"Эфир не существует",
		})
		return
	}

	p.scheduleStorage.EditSchedule(form.Index, form.Begin, form.End, form.Title, form.Dj)

	toJson(w, struct {
		Status string `json:"status"`
	}{
		"ok",
	})
}

type NewScheduleForm struct {
	Begin int64  `schema:"begin"`
	End   int64  `schema:"end"`
	Title string `schema:"title"`
	Dj    string `schema:"dj"`
}

type DeleteScheduleForm struct {
	Index int `schema:"index"`
}

type EditScheduleForm struct {
	Index int    `schema:"index"`
	Begin int64  `schema:"begin"`
	End   int64  `schema:"end"`
	Title string `schema:"title"`
	Dj    string `schema:"dj"`
}

package bot

import "net/http"

func (p *RadioStation) APIAirState(w http.ResponseWriter, r *http.Request) {
	toJson(w, UserAirState{
		Status:     p.airState,
		LiveStatus: p.airLiveState,
	})
}

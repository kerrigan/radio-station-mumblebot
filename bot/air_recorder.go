package bot

import (
	"encoding/json"
	"io"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"sync"
	"time"
)

type RadioStreamSaver struct {
	client        *http.Client
	c             chan int
	mountURL      string
	started       bool
	trackingFile  io.Writer
	airTimestamps []AirTimestamp
	lock          sync.Locker
}

func NewRadioStreamSaver(mountURL string) *RadioStreamSaver {
	return &RadioStreamSaver{
		client:        &http.Client{},
		c:             make(chan int),
		mountURL:      mountURL,
		started:       false,
		lock:          &sync.Mutex{},
		trackingFile:  nil,
		airTimestamps: []AirTimestamp{},
	}
}

func createOrOpenFile(path string) (trackingOutputFile *os.File, filename string, fileError error) {
	var f *os.File

	now := time.Now().Format("2006-01-02_15:04:05")

	filename = filepath.Join(path, now)

	f, err := os.Create(filepath.Join(path, now+".json"))

	if err != nil {
		fileError = err
		return
	}
	trackingOutputFile = f

	return
}

func (p *RadioStreamSaver) Start(path string) error {
	defer func() {
		p.lock.Unlock()
	}()

	p.lock.Lock()

	if p.started {
		return nil
	}

	p.started = true

	trackingFile, filename, err := createOrOpenFile(path)

	if err != nil {
		log.Println(err)
		p.started = false
		return err
	}

	p.trackingFile = trackingFile

	StartRecord(filename, func() {

	})

	return nil
}

func (p *RadioStreamSaver) Stop() {
	defer func() {
		p.lock.Unlock()
	}()

	p.lock.Lock()

	StopRecord(func() {})

	p.started = false

	if p.trackingFile == nil {
		log.Println("Record file not opened")
		return
	}

	trackEncoder := json.NewEncoder(p.trackingFile)
	err := trackEncoder.Encode(p.airTimestamps)

	if err != nil {
		log.Println(err)
	}
}

func (p *RadioStreamSaver) ClearTimestamps() {
	p.airTimestamps = []AirTimestamp{
		AirTimestamp{
			Timestamp: 0,
			Label:     "Начало эфира",
		},
	}
}

package bot

import (
	"fmt"
	"html"
	"log"
	"sort"
	"strings"
	"sync"

	utils "bitbucket.org/kerrigan/bookfags-radio/utils"
	mumbot "bitbucket.org/kerrigan/gomumbot/mumbot"
)

type SiteMessageHandler struct {
	logStorage  *utils.LogStorage
	bot         *mumbot.MumbleBot
	ctrlChannel chan int
	lock        sync.Mutex
}

func (p *RadioStation) ProcessMessage(bot *mumbot.MumbleBot, user *mumbot.User, private bool, message string) {
	if !private {
		p.logStorage.AddMessage(user.Nick, message, false, "")
	} else {

		switch {
		case message == ".stats":
			go p.getIcecastsStats(bot, user)
		case strings.HasPrefix(message, ".ban "):
			args := strings.SplitN(message, " ", 2)
			if len(args) == 1 {
				bot.WritePrivateMessage(user.ID, "Usage: .ban USERID")
				return
			}

			siteUserId := args[1]

			p.aclStorage.BanUser(siteUserId)
			log.Println(siteUserId, "now banned")
		case strings.HasPrefix(message, ".unban "):
			args := strings.SplitN(message, " ", 2)
			if len(args) == 1 {
				bot.WritePrivateMessage(user.ID, "Usage: .ban USERID")
				return
			}

			siteUserId := strings.TrimSpace(args[1])
			p.aclStorage.UnbanUser(siteUserId)
			log.Println(siteUserId, "now unbanned")
		case strings.HasPrefix(message, ".banip "):
			args := strings.SplitN(message, " ", 2)
			if len(args) == 1 {
				p.bot.WritePrivateMessage(user.ID, "Usage: .banip IP")
				return
			}

			IP := strings.TrimSpace(args[1])
			p.aclStorage.BanIP(IP)
			log.Println(IP, "now banned")
		case strings.HasPrefix(message, ".unbanip "):
			args := strings.SplitN(message, " ", 2)
			if len(args) == 1 {
				p.bot.WritePrivateMessage(user.ID, "Usage: .unbanip IP")
				return
			}

			IP := args[1]

			p.aclStorage.UnbanIP(IP)
			log.Println(IP, "now unbanned")

		case strings.HasPrefix(message, ".banned"):
			p.bot.WritePrivateMessage(user.ID,
				fmt.Sprintf("Users:<br>%s<br>IPs:<br>%s",
					strings.Join(p.aclStorage.GetBannedUsers(), "<br>"),
					strings.Join(p.aclStorage.GetBannedIPs(), "<br>")))
		case strings.HasPrefix(message, ".air"):
			if p.InWhiteList(user.Nick) {
				p.startAir()
			}
		case strings.HasPrefix(message, ".mnt"):
			if p.InWhiteList(user.Nick) {
				p.startDj()
			}
		case strings.HasPrefix(message, ".mpd"):
			if p.InWhiteList(user.Nick) {
				p.startMpd()
			}
		}

	}
}

func (p *RadioStation) UserStateChanged(b *mumbot.MumbleBot, users map[uint32]*mumbot.User) {
	usersnicks := make([]string, 0)
	botUserID := b.GetUserID()

	for uid, user := range users {
		if uid != botUserID && !p.InLogBlackList(user.Nick) && user.ChannelID == users[botUserID].ChannelID {
			usersnicks = append(usersnicks, user.Nick)
		}
	}

	sort.Strings(usersnicks)
	p.logStorage.ChangeUsers(usersnicks)
}

func (p *RadioStation) processIncoming() {
	for {
		var message utils.HearerMessage
		select {
		case message = <-p.logStorage.HearerMessageBus:
		case <-p.ctrlChannel:
			return
		}

		text := html.EscapeString(message.Text)

		var messageStr string
		if message.DJOnly {
			messageStr = fmt.Sprintf(`<span style="background-color: #fea308;color:black;">[DJ]</span><font color='red'>%s@</font>%s&gt; <font color='red'>%s</font>`, message.Nick, message.ColorIP(), text)
		} else if message.IsAdmin {
			messageStr = fmt.Sprintf(`<font color='green'>%s</font>&gt; <font color='red'>%s</font>`, message.Nick, text)
		} else {
			messageStr = fmt.Sprintf(`<font color='red'>%s@</font>%s&gt; <font color='red'>%s</font>`, message.Nick, message.ColorIP(), text)
		}
		p.bot.WriteMessage(messageStr)
	}
}

func (p *RadioStation) StopMessageHandler() {
	p.ctrlChannel <- 1
}

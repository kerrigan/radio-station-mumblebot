package bot

func (p *RadioStation) startLiveVoice() {
	StartLiveVoiceAir(func() {
		p.airLiveState = LIVE_VOICE

		data := WSPayload{
			Type: "airstate",
			Payload: AirState{
				AirStarted:    p.airStarted,
				Mode:          p.airState,
				LiveMode:      p.airLiveState,
				AirTimestamps: p.airRecorder.airTimestamps,
			},
		}

		//TODO: run record

		p.adminWsBroadcast(data)
	})
}

func (p *RadioStation) startLiveVoiceMpd() {
	StartLiveVoiceMpdAir(func() {
		p.airLiveState = LIVE_VOICE_MPD

		data := WSPayload{
			Type: "airstate",
			Payload: AirState{
				AirStarted:    p.airStarted,
				Mode:          p.airState,
				LiveMode:      p.airLiveState,
				AirTimestamps: p.airRecorder.airTimestamps,
			},
		}

		p.adminWsBroadcast(data)
	})
}

func (p *RadioStation) startLiveMpd() {
	StartLiveMpdAir(func() {
		p.airLiveState = LIVE_MPD

		p.notifyStartAir()

		data := WSPayload{
			Type: "airstate",
			Payload: AirState{
				AirStarted:    p.airStarted,
				Mode:          p.airState,
				LiveMode:      p.airLiveState,
				AirTimestamps: p.airRecorder.airTimestamps,
			},
		}

		p.adminWsBroadcast(data)
	})
}

package bot

import (
	"net"
	"os"
	"os/exec"
	"sync"

	utils "bitbucket.org/kerrigan/bookfags-radio/utils"
	mumbot "bitbucket.org/kerrigan/gomumbot/mumbot"
	"github.com/gorilla/sessions"
	"github.com/gorilla/websocket"
)

type RadioStation struct {
	bot *mumbot.MumbleBot
	//mHandler mumbot.MessageHandler

	aclStorage      *utils.ACLStorage
	logStorage      *utils.LogStorage
	scheduleStorage *utils.ScheduleStorage
	iceUserStorage  *utils.IcecastAuthStorage
	authStorage     utils.AuthStorage
	roleStorage     *utils.RoleStorage
	iframeStorage   *utils.IFrameStorage

	config *Config

	//Web
	wsConnections      map[net.Addr]*websocket.Conn
	adminWsConnections map[net.Addr]*websocket.Conn

	airStarted   int64
	airState     string
	airLiveState string

	sessionStore sessions.Store

	//Control channels
	ctrlChannel chan int
	lock        sync.Locker

	captchaTimeoutLock  sync.Locker
	captchaTimeoutStore map[string]bool
	//Liquidsoap
	lsProcess *exec.Cmd

	airRecorder *RadioStreamSaver
}

func NewRadioStation(config *Config) *RadioStation {
	os.Mkdir("data", 0755)

	aclStorage := utils.NewACLStorage()
	logStorage := utils.NewLogStorage()
	scheduleStorage := utils.NewScheduleStorage()
	iceUserStorage := utils.NewIcecastAuthStorage()
	plainAuthStorage := utils.NewPlainAuthStorage()
	plainRoleStorage := utils.NewRoleStorage()

	bot := mumbot.NewMumbleBot(config.MumbleHost, config.MumblePort, config.Nick)

	sessionStore := sessions.NewCookieStore([]byte(config.SecretSalt))

	iframeStorage := utils.NewIFrameStorage()

	station := &RadioStation{
		config:          config,
		bot:             bot,
		aclStorage:      aclStorage,
		logStorage:      logStorage,
		scheduleStorage: scheduleStorage,
		iceUserStorage:  iceUserStorage,
		authStorage:     &plainAuthStorage,
		roleStorage:     &plainRoleStorage,
		iframeStorage:   iframeStorage,

		airRecorder: NewRadioStreamSaver(config.AudioStreamUrl),

		//Web
		wsConnections:      make(map[net.Addr]*websocket.Conn),
		adminWsConnections: make(map[net.Addr]*websocket.Conn),
		sessionStore:       sessionStore,

		captchaTimeoutLock:  &sync.Mutex{},
		captchaTimeoutStore: make(map[string]bool),

		ctrlChannel: make(chan int),
		lock:        &sync.Mutex{},

		airState:     MPD_STARTED,
		airLiveState: LIVE_VOICE,
	}

	bot.SetMessageHandler(station)
	bot.SetUserStateHandler(station)

	return station
}

func (p *RadioStation) Run() {
	go p.RunHttp(p.config.WebPort)
	go p.processIncoming()
	p.bot.Run()
}

func (p *RadioStation) Stop() {
	p.bot.Stop()
}

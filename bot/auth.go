package bot

import (
	"net/http"
	"sort"

	"bitbucket.org/kerrigan/bookfags-radio/utils"
)

/*
func (p *RadioStation) L(w http.ResponseWriter, r *http.Request) bool {
	session, _ := p.sessionStore.Get(r, "session")

	if loggedIn, ok := session.Values["login"]; ok {
		if loggedIn == "admin" {
			return true
		}
	}

	http.Error(w, "Unauthorized", http.StatusForbidden)
	return false
}
*/

// LM - simple auth middleware without roles
func (p *RadioStation) LM(handlerFunc func(w http.ResponseWriter, r *http.Request)) http.Handler {
	return p.LMR(handlerFunc, []utils.Role{})
}

// LMR - auth middleware with roles
func (p *RadioStation) LMR(handlerFunc func(w http.ResponseWriter, r *http.Request), roles []utils.Role) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		session, _ := p.sessionStore.Get(r, "session")

		if sessionIDVal, ok := session.Values["session"]; ok {
			sessionID := sessionIDVal.(string)
			//TODO: check roles
			if userID, err := p.authStorage.GetUserID(sessionID); err == nil {
				userRoles := p.roleStorage.GetRoles(userID)
				neededRolesCount := len(roles)
				rolesCount := 0

				if neededRolesCount > 0 {
					for _, role := range roles {
						for _, userRole := range userRoles {
							if role == userRole {
								rolesCount++
								continue
							}
						}
					}
				}

				if rolesCount == neededRolesCount {
					handlerFunc(w, r)
					return
				}
			}
		}

		http.Error(w, "Unauthorized", http.StatusForbidden)
	})
}

func (p *RadioStation) InWhiteList(mumbleNick string) bool {
	if sort.SearchStrings(p.config.WhiteList, mumbleNick) < len(p.config.WhiteList) {
		return true
	}
	return false
}

func (p *RadioStation) InLogBlackList(mumbleNick string) bool {
	if sort.SearchStrings(p.config.LogBlackList, mumbleNick) < len(p.config.LogBlackList) {
		return true
	}

	return false
}

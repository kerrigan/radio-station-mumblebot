package bot

import (
	"net/http"

	"bitbucket.org/kerrigan/bookfags-radio/utils"
	"github.com/gorilla/schema"
)

func (p *RadioStation) APIExternalWidget(w http.ResponseWriter, r *http.Request) {
	toJson(w, p.iframeStorage.Config)
}

func (p *RadioStation) APIAdminExternalWidget(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()

	if r.Method == http.MethodPost {
		form := new(NewIFrameForm)
		var decoder = schema.NewDecoder()
		err := decoder.Decode(form, r.PostForm)

		if err != nil {
			http.Error(w, "Wrong height", http.StatusBadRequest)
			return
		}

		p.iframeStorage.SetIFrameSettings(form.URL, form.Height)

		p.wsBroadcast(struct {
			Type string             `json:"type"`
			Data utils.IFrameConfig `json:"data"`
		}{
			Type: "widget",
			Data: *p.iframeStorage.Config,
		})

	} else if r.Method == http.MethodDelete {
		p.iframeStorage.ClearIframe()

		data := struct {
			Action string `json:"action"`
		}{
			Action: "delete",
		}

		p.wsBroadcast(struct {
			Type string      `json:"type"`
			Data interface{} `json:"data"`
		}{
			Type: "widget",
			Data: data,
		})
	}

	toJson(w, p.iframeStorage.Config)

}

type NewIFrameForm struct {
	URL    string `schema:"url"`
	Height int    `schema:"height"`
}

var app = angular.module('radioStation', ['ngSanitize', 'twitter.timeline', 'luegg.directives']);

function wsproto(){
     return location.protocol.match(/^https/) ? "wss" : "ws";
}


app.config(['$httpProvider', function ($httpProvider) {
    $httpProvider.defaults.transformRequest = function(data){
        if (data === undefined) {
            return data;
        }
        return $.param(data);
    };
    $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
}]);

app.filter('toHMS', function() {
  return function(input) {
      var seconds = input % 60;
      var minutes = parseInt(input / 60) % 60;
      var hours = parseInt(input / 3600) % 24;
      var data = (hours < 10 ? "0" + hours : hours) + ":" + (minutes  < 10 ? "0" + minutes : minutes) + ":" + (seconds < 10 ? "0" + seconds : seconds);
      return data;
  };
});

function format0prefix(value) {
  return (value < 10 ? "0" + value : value + "");
}

app.filter('fromUnix', function() {
  return function(input) {
    var timestamp = new Date(input * 1000);
    var seconds = timestamp.getSeconds();
    var minutes = timestamp.getMinutes();
    var hours = timestamp.getHours();
    var time = (hours < 10 ? "0" + hours : hours) + ":" + (minutes  < 10 ? "0" + minutes : minutes) + ":" + (seconds < 10 ? "0" + seconds : seconds);

    var day = timestamp.getDate();
    var month = timestamp.getMonth() + 1;
    var year = timestamp.getFullYear();
    var date = format0prefix(day) + "." + format0prefix(month) + "." + year;
    var data = date + " " + time;
    return data;
  };
});


app.filter('dateFromUnix', function() {
  return function(input) {
    var timestamp = new Date(input * 1000);

    var day = timestamp.getDate();
    var month = timestamp.getMonth() + 1;
    var year = timestamp.getFullYear();
    var date = format0prefix(day) + "." + format0prefix(month) + "." + year;
    return date;
  };
})

app.filter('timeFromUnix', function() {
  return function(input) {
    var timestamp = new Date(input * 1000);
    var seconds = timestamp.getSeconds();
    var minutes = timestamp.getMinutes();
    var hours = timestamp.getHours();
    var time = (hours < 10 ? "0" + hours : hours) + ":" + (minutes  < 10 ? "0" + minutes : minutes) + ":" + (seconds < 10 ? "0" + seconds : seconds);
    return time;
  };
})

app.factory('audio', ['$document', function($document) {
  var audio = $document[0].createElement('audio');
  return audio;
}]);

app.factory('UserWsService', ['$rootScope', '$interval', function($rootScope, $interval){

    var listeners = [];

    var ws = new ReconnectingWebSocket(wsproto() + '://' + window.location.host + '/ws');


    var pingTimer;

    ws.onopen = function(){
        console.log("Connection established");
        pingTimer = $interval(function(){
            ws.send("ping");
        }, 5000);
    }

    ws.onmessage = function(event){
        var data = JSON.parse(event.data);
        listeners.forEach(function(listener){
            //safeApply(function(){
            $rootScope.$apply(function(){
                listener(data);
            });

            //});
        });
    };


    ws.onclose = function(){
        $interval.cancel(pingTimer);
        //TODO: make reconnect by timeout
    };

    var Service = {};

    Service.register = function(listener){
        listeners.push(listener);
    };
    return Service;
}]);


app.factory('AdminWsService', ['$rootScope', '$interval', function($rootScope, $interval){
    var listeners = [];

    var ws = new ReconnectingWebSocket(wsproto() + '://' + window.location.host + '/admin/ws');

    var pingTimer;

    ws.onopen = function(){
        console.log("Connection established");
        pingTimer = $interval(function(){
            ws.send("ping");
        }, 5000);
    };

    ws.onclose = function(){
        $interval.cancel(pingTimer);
        //TODO: make reconnect by timeout
    };

    ws.onmessage = function(event){
        var data = JSON.parse(event.data);
        listeners.forEach(function(listener){
            $rootScope.$apply(function(){
                listener(data);
            });
        });
    };

    var Service = {};

    Service.register = function(listener){
        listeners.push(listener);
    };
    return Service;
}]);

app.directive('iframeOnload', function(){
  return {
    scope: {
      callBack: '&iframeOnload'
    },
    restrict: 'A',
    link: function(scope, element, attrs) {
      element.on('load', function() {
        console.log('in directive iframe loaded',element);
        // bad bad bad!
        // var elemWrapper = {theElem:element};
        // return scope.callBack({element:elemWrapper});

        var iFrameHeight = element[0].contentWindow.document.body.scrollHeight + 'px';
        var iFrameWidth = '100%';
        element.css('width', iFrameWidth);
        element.css('height', iFrameHeight);

        return scope.callBack({element: element});
      });
    }
  };
});


app.directive('resized', function(){
  return {
    scope: {
      callback: '&resized'
    },
    restrict: 'A',
    link: function(scope, element, attrs) {
      var ro = new ResizeObserver( entries => {
        for (let entry of entries) {
          const cr = entry.contentRect;
          scope.callback({size: cr});
        }
      });

      ro.observe(element[0]);
    }
  }
})

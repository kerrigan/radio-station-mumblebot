#!/bin/sh

clean:
	rm -f bot/*.rice-box.go
	rm -f bot/rice-box.go

build:
	go build

dist: clean build
	mkdir -p dist
	cp bookfags-radio dist/
	cp -r templates dist/
	cp -r static dist/
	cp config.json.example dist/config.json

create_inline_resources:
	go get github.com/GeertJohan/go.rice
	go get github.com/GeertJohan/go.rice/rice
	$(GOPATH)/bin/rice --import-path=bitbucket.org/kerrigan/bookfags-radio/bot embed-go

inlinedist: clean create_inline_resources build
	mkdir -p inlinedist
	cp bookfags-radio inlinedist/
	cp config.json.example inlinedist/

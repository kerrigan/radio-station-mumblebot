package utils

import (
	"testing"
)

func TestPlainAuthMethod(t *testing.T) {
	storage := NewPlainAuthMethod("test", "testpass")
	if len(storage.accounts) == 0 {
		t.Error("Expected accounts")
	}
}

package utils

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"log"
	"sync"

	"github.com/google/uuid"
)

type AuthStorage interface {
	Login(authmethod AuthMethod) (string, error)
	Logout(sessionID string)
	GetUserID(sessionID string) (string, error)
}

type PlainAuthStorage struct {
	filename string
	lock     *sync.RWMutex
	sessions map[string]string
}

func NewPlainAuthStorage() PlainAuthStorage {
	storage := PlainAuthStorage{
		lock:     &sync.RWMutex{},
		sessions: make(map[string]string),
		filename: "data/sessions.json",
	}
	storage.load()
	return storage
}

func (p *PlainAuthStorage) Login(authmethod AuthMethod) (session string, authError error) {
	if authmethod.Login() {
		sessionId := uuid.New().String()
		p.lock.Lock()
		p.sessions[sessionId] = authmethod.UserID()
		p.save()
		defer p.lock.Unlock()
		return sessionId, nil
	}
	return "", errors.New("Auth failed")
}

func (p *PlainAuthStorage) GetUserID(session string) (string, error) {
	p.load()

	p.lock.RLock()
	defer p.lock.RUnlock()
	if userID, ok := p.sessions[session]; ok {
		return userID, nil
	}

	return "", errors.New("Session not found")
}

func (p *PlainAuthStorage) Logout(sessionID string) {
	p.lock.Lock()
	defer p.lock.Unlock()
	delete(p.sessions, sessionID)
	p.save()
}

func (p *PlainAuthStorage) save() {
	data, err := json.Marshal(p.sessions)
	if err != nil {
		return
	}

	ioutil.WriteFile(p.filename, data, 0644)
}

func (p *PlainAuthStorage) load() {

	data, err := ioutil.ReadFile(p.filename)

	if err != nil {
		log.Println("Failed to read sessions, create new")
	}

	sessions := make(map[string]string)

	err = json.Unmarshal(data, &sessions)

	p.sessions = sessions

	if err != nil {
		log.Println("Failed to parse sessions, create new")
		p.save()
	}
}

package utils

import (
	"encoding/json"
	"io/ioutil"
	"strings"
)

const MESSAGE_ID_LEN = 8

type LogStorage struct {
	Messages           []LogMessage
	MessageBus         chan LogMessage
	HearerMessageBus   chan HearerMessage
	HearerMessages     []HearerMessage
	HearerToDJMessages []HearerMessage
	Users              []string
	UsersBus           chan []string
}

var instance *LogStorage = nil

func ReplaceUrls(html string) string {
	return strings.Replace(html, "<a href", "<a target='blank' href", 1)
}

func NewLogStorage() *LogStorage {
	if instance == nil {
		instance = &LogStorage{
			Messages:           make([]LogMessage, 0),
			MessageBus:         make(chan LogMessage, 1024),
			HearerMessages:     make([]HearerMessage, 0),
			HearerMessageBus:   make(chan HearerMessage, 1024),
			HearerToDJMessages: make([]HearerMessage, 0),
			Users:              make([]string, 0),
			UsersBus:           make(chan []string, 1024),
		}
		instance.loadLogs()
	}

	return instance
}

func (p *LogStorage) AddMessage(nick, text string, isHearer bool, IP string) {
	if len(p.Messages) == 20 {
		p.Messages = p.Messages[1:]
	}
	msg := NewLogMessage(nick, text, IP, isHearer)
	p.Messages = append(p.Messages, msg)
	p.saveLogs()
	p.MessageBus <- msg
}

func (p *LogStorage) AddHearerToDJMessage(nick, text, IP string) {
	msg := NewHearerToDJMessage(nick, text, IP)
	if len(p.HearerToDJMessages) == 60 {
		p.HearerToDJMessages = p.HearerToDJMessages[1:]
	}

	p.HearerToDJMessages = append(p.HearerToDJMessages, msg)
	p.saveLogs()
	p.HearerMessageBus <- msg
}

func (p *LogStorage) AddAdminMessage(text string) {
	if len(p.HearerMessages) == 20 {
		p.HearerMessages = p.HearerMessages[1:]
	}
	adminName := "Администратор"
	msg := NewAdminMessage(text)
	p.HearerMessages = append(p.HearerMessages, msg)
	p.HearerMessageBus <- msg
	p.AddMessage(adminName, text, false, "")
}

func (p *LogStorage) AddHearerMessage(nick, text, IP string) {
	if len(p.HearerMessages) == 20 {
		p.HearerMessages = p.HearerMessages[1:]
	}
	msg := NewHearerMessage(nick, text, IP)
	p.HearerMessages = append(p.HearerMessages, msg)
	p.HearerMessageBus <- msg
	p.AddMessage(nick, text, true, IP)
}

func (p *LogStorage) ChangeUsers(users []string) {
	p.Users = users
	p.UsersBus <- p.Users
}

func (p *LogStorage) saveLogs() {
	data, err := json.Marshal(p.Messages)

	if err != nil {
		panic(err)
	}

	err = ioutil.WriteFile("data/logs.json", data, 0644)
	if err != nil {
		panic(err)
	}

	data, err = json.Marshal(p.HearerToDJMessages)

	if err != nil {
		panic(err)
	}

	err = ioutil.WriteFile("data/djfeedback.json", data, 0644)
	if err != nil {
		panic(err)
	}
}

func (p *LogStorage) loadLogs() {
	//Load logs
	var logs []LogMessage
	data, err := ioutil.ReadFile("data/logs.json")

	if err != nil {
		return
	}

	err = json.Unmarshal(data, &logs)

	if err != nil {
		return
	}

	for index := 0; index < len(logs); index++ {
		if logs[index].ID == "" {
			logs[index].ID = RandStringRunes(MESSAGE_ID_LEN)
		}
	}

	p.Messages = logs

	//Load feedbacks

	var feedbacks []HearerMessage
	data, err = ioutil.ReadFile("data/djfeedvback.json")

	if err != nil {
		return
	}

	err = json.Unmarshal(data, &feedbacks)

	if err != nil {
		return
	}

	p.HearerToDJMessages = feedbacks
}

/*
func (p *LogStorage) DeleteMessage(message LogMessage) {
	b := p.Messages[:0]
	for _, x := range p.Messages {
		if !x.Equal(message) {
			b = append(b, x)
		}
	}
	p.Messages = b

	p.saveLogs()
}
*/

func (p *LogStorage) DeleteMessage(messageID string) {
	b := p.Messages[:0]
	for _, x := range p.Messages {
		if !(x.ID == messageID) {
			b = append(b, x)
		}
	}
	p.Messages = b

	p.saveLogs()
}

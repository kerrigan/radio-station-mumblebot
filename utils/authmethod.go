package utils

type AuthMethod interface {
	Login() bool
	UserID() string
}

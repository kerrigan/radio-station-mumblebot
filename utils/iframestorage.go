package utils

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"sync"
)

type IFrameConfig struct {
	Height int    `json:"height"`
	Url    string `json:"url"`
}

type IFrameStorage struct {
	Config    *IFrameConfig
	ConfigBus chan IFrameConfig
	lock      sync.Locker
}

func NewIFrameStorage() *IFrameStorage {
	instance := &IFrameStorage{
		Config:    nil,
		ConfigBus: make(chan IFrameConfig, 1024),
		lock:      &sync.Mutex{},
	}

	instance.load()

	return instance
}

func (p *IFrameStorage) SetIFrameSettings(url string, height int) {
	newConfig := IFrameConfig{
		Height: height,
		Url:    url,
	}
	p.Config = &newConfig
	p.save()
}

func (p *IFrameStorage) ClearIframe() {
	p.Config = nil
	p.save()
}

func (p *IFrameStorage) load() {
	var config IFrameConfig

	data, err := ioutil.ReadFile("data/iframe.json")

	if err != nil {
		log.Println("Iframe config not found")
		return
	}

	err = json.Unmarshal(data, &config)

	if err != nil {
		log.Print("Iframe config load failed")
		return
	}
	p.Config = &config
}

func (p *IFrameStorage) save() {
	data, err := json.Marshal(p.Config)
	if err != nil {
		panic(err)
	}

	err = ioutil.WriteFile("data/iframe.json", data, 0644)
	if err != nil {
		panic(err)
	}
}

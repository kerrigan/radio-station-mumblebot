package utils

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"sync"
)

type IcecastAuthStorage struct {
	Users    IcecastUsers
	UsersBus chan IcecastUsers
	lock     sync.Locker
}

type IcecastUser struct {
	Login    string `json:"login"`
	Password string `json:"password"`
	Blocked  bool   `json:"blocked"`
}

type IcecastUsers []IcecastUser

func NewIcecastAuthStorage() *IcecastAuthStorage {
	instance := &IcecastAuthStorage{
		Users:    make(IcecastUsers, 0),
		UsersBus: make(chan IcecastUsers, 1024),
		lock:     &sync.Mutex{},
	}

	instance.loadUsers()

	return instance
}

func (p *IcecastAuthStorage) loadUsers() {
	var users IcecastUsers

	data, err := ioutil.ReadFile("data/djs.json")

	if err != nil {
		log.Println("Djs credentials not found")
		return
	}

	err = json.Unmarshal(data, &users)

	if err != nil {
		log.Print("Djs credentials load failed")
		return
	}
	p.Users = users
}

func (p *IcecastAuthStorage) saveUsers() {
	data, err := json.Marshal(p.Users)
	if err != nil {
		panic(err)
	}

	err = ioutil.WriteFile("data/djs.json", data, 0644)
	if err != nil {
		panic(err)
	}
}

func (p *IcecastAuthStorage) AddUser(login, password string) {
	p.lock.Lock()
	p.Users = append(p.Users, IcecastUser{Login: login, Password: password, Blocked: false})
	p.lock.Unlock()

	p.saveUsers()
	p.UsersBus <- p.Users
}

func (p *IcecastAuthStorage) RemoveUser(index int) {
	if index >= len(p.Users) {
		return
	}

	p.lock.Lock()
	p.Users = append(p.Users[:index], p.Users[index+1:]...)
	p.lock.Unlock()
	p.saveUsers()
	p.UsersBus <- p.Users
}

func (p *IcecastAuthStorage) EditUser(index int, login, password string, blocked bool) {
	if index >= len(p.Users) {
		return
	}

	p.lock.Lock()
	p.Users[index] = IcecastUser{
		Login:    login,
		Password: password,
		Blocked:  blocked,
	}
	p.lock.Unlock()
	p.saveUsers()
	p.UsersBus <- p.Users
}

func (p *IcecastAuthStorage) CheckUser(login, password string) bool {
	for _, user := range p.Users {
		if login == user.Login && password == user.Password {
			if user.Blocked {
				return false
			}
			return true

		}
	}
	return false
}

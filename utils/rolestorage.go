package utils

import (
	"encoding/json"
	"io/ioutil"
	"sync"
)

type Role uint16

const (
	CHAT_EDITOR     Role = 0
	SCHEDULE_EDITOR Role = 1
	AIR_MANAGER     Role = 2
)

type RoleStorage struct {
	lock  sync.Locker
	roles map[string][]Role
}

func NewRoleStorage() RoleStorage {
	result := RoleStorage{
		roles: make(map[string][]Role),
		lock:  &sync.Mutex{},
	}

	result.load()

	return result
}

func (p *RoleStorage) load() {
	data, err := ioutil.ReadFile("data/roles.json")
	if err == nil {
		p.lock.Lock()
		json.Unmarshal(data, &p.roles)
		p.lock.Unlock()
	}
}

func (p RoleStorage) GetRoles(userID string) []Role {
	p.load()

	p.lock.Lock()
	defer p.lock.Unlock()
	if roles, ok := p.roles[userID]; ok {
		return roles
	}
	return []Role{}
}

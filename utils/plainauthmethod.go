package utils

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
)

type PlainAuthMethod struct {
	username string
	password string
	accounts map[string]PlainAuthRecord
}

type PlainAuthRecord struct {
	Password string `json:"password"`
}

func NewPlainAuthMethod(username string, password string) (result PlainAuthMethod) {
	result = PlainAuthMethod{
		username: username,
		password: password,
		accounts: make(map[string]PlainAuthRecord),
	}

	data, err := ioutil.ReadFile("data/plainauth.json")

	if err != nil {
		return
	}

	json.Unmarshal(data, &result.accounts)

	return result
}

func (p PlainAuthMethod) UserID() string {
	return fmt.Sprintf("PLAIN:%s", p.username)
}

func (p PlainAuthMethod) Login() bool {
	if account, ok := p.accounts[p.username]; ok {
		if account.Password == p.password {
			return true
		}
	}
	return false
}

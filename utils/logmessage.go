package utils

import (
	"fmt"
	"strconv"
	"strings"
	"time"
)

type LogMessage struct {
	ID string `json:"id"`
	//Timestamp time.Time `json:"timestamp"`
	Time     int64  `json:"time"`
	Nick     string `json:"nick"`
	Text     string `json:"text"`
	IsHearer bool   `json:"isHearer"`
	IP       string `json:"ip"`
}

// LogMessage constructor
func NewLogMessage(nick, text, IP string, isHearer bool) LogMessage {
	return LogMessage{
		ID:       RandStringRunes(MESSAGE_ID_LEN),
		Time:     time.Now().Unix(),
		Nick:     nick,
		Text:     ReplaceUrls(text),
		IP:       IP,
		IsHearer: isHearer,
	}
}

func (p LogMessage) Equal(m LogMessage) bool {
	return p.Time == m.Time && p.Nick == m.Nick && p.Text == m.Text
}

type HearerMessage struct {
	Timestamp time.Time
	Nick      string
	Text      string
	IP        string
	DJOnly    bool
	IsAdmin   bool
}

func NewHearerToDJMessage(nick, text, IP string) HearerMessage {
	return HearerMessage{
		Timestamp: time.Now(),
		Nick:      nick,
		Text:      text,
		IP:        IP,
		DJOnly:    true,
		IsAdmin:   false,
	}
}

func NewHearerMessage(nick, text, IP string) HearerMessage {
	return HearerMessage{
		Timestamp: time.Now(),
		Nick:      nick,
		Text:      text,
		IP:        IP,
		DJOnly:    false,
		IsAdmin:   false,
	}
}

func NewAdminMessage(text string) HearerMessage {
	adminName := "Администратор"

	return HearerMessage{
		Timestamp: time.Now(),
		Nick:      adminName,
		Text:      text,
		IP:        "",
		DJOnly:    false,
		IsAdmin:   false,
	}
}

func (p *HearerMessage) ColorIP() string {
	parts := strings.SplitN(p.IP, ".", 4)

	result := ""

	for _, part := range parts {
		i, err := strconv.Atoi(part)

		if err == nil {
			i = i % 216
			result += fmt.Sprintf(`<span style="background-color:%s;color:%s;">a</span>`, WEB_SAFE_COLORS[i], WEB_SAFE_COLORS[i])
		}
	}

	return result
}

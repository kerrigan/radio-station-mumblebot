package utils

import (
	"encoding/json"
	"io/ioutil"
	"sort"
	"sync"
	"time"
)

type Schedule struct {
	Begin int64 `json:"begin"`
	End   int64 `json:"end"`

	Title string `json:"title"`
	Dj    string `json:"dj"`

	Finished bool `json:"finished"`
}

type Schedules []Schedule

func (v Schedules) Len() int {
	return len(v)
}

func (v Schedules) Swap(i, j int) {
	v[i], v[j] = v[j], v[i]
}

// default comparison.
func (v Schedules) Less(i, j int) bool {
	return v[i].Begin < v[j].Begin
}

type ScheduleStorage struct {
	Schedules   Schedules
	ScheduleBus chan []Schedule
	lock        sync.Locker
}

func NewScheduleStorage() *ScheduleStorage {
	instance := &ScheduleStorage{
		Schedules:   make([]Schedule, 0),
		ScheduleBus: make(chan []Schedule, 1024),
		lock:        &sync.Mutex{},
	}

	instance.loadSchedules()

	return instance
}

func (p *ScheduleStorage) loadSchedules() {
	var schedules []Schedule
	data, err := ioutil.ReadFile("data/schedules_v2.json")

	if err != nil {
		return
	}

	err = json.Unmarshal(data, &schedules)

	if err != nil {
		return
	}
	p.Schedules = schedules
}

func (p *ScheduleStorage) GetSchedules() Schedules {
	for i := range p.Schedules {
		if p.Schedules[i].End < time.Now().Unix() {
			p.Schedules[i].Finished = true
		} else {
			p.Schedules[i].Finished = false
		}
	}
	return p.Schedules
}

func (p *ScheduleStorage) saveSchedules() {
	for i := range p.Schedules {
		if p.Schedules[i].End < time.Now().Unix() {
			p.Schedules[i].Finished = true
		} else {
			p.Schedules[i].Finished = false
		}
	}

	data, err := json.Marshal(p.Schedules)

	if err != nil {
		panic(err)
	}

	err = ioutil.WriteFile("data/schedules_v2.json", data, 0644)
	if err != nil {
		panic(err)
	}
}

func (p *ScheduleStorage) AddSchedule(begin, end int64, title, dj string) {
	//build from datetime
	p.lock.Lock()
	p.Schedules = append(
		p.Schedules,
		Schedule{
			Begin: begin,
			End:   end,
			Title: title,
			Dj:    dj,
		},
	)
	//and sort by date
	sort.Sort(p.Schedules)
	p.lock.Unlock()

	p.saveSchedules()
	p.ScheduleBus <- p.Schedules
}

func (p *ScheduleStorage) RemoveSchedule(index int) {
	if index >= len(p.Schedules) {
		return
	}

	p.lock.Lock()
	p.Schedules = append(p.Schedules[:index], p.Schedules[index+1:]...)
	p.lock.Unlock()
	p.saveSchedules()
	p.ScheduleBus <- p.Schedules
}

func (p *ScheduleStorage) EditSchedule(index int, begin, end int64, title, dj string) {
	if index >= len(p.Schedules) {
		return
	}

	p.lock.Lock()
	p.Schedules[index] = Schedule{
		Begin: begin,
		End:   end,
		Title: title,
		Dj:    dj,
	}
	sort.Sort(p.Schedules)
	p.lock.Unlock()
	p.saveSchedules()
	p.ScheduleBus <- p.Schedules
}

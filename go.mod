module bitbucket.org/kerrigan/bookfags-radio

go 1.14

require bitbucket.org/kerrigan/gomumbot v0.0.0-20200517205012-dd4ced1f42f1

require github.com/GeertJohan/go.rice v1.0.0

require github.com/dchest/captcha v0.0.0-20170622155422-6a29415a8364

require github.com/gorilla/mux v1.6.2

require github.com/gorilla/sessions v1.1.3

require github.com/gorilla/websocket v1.4.0

require (
	github.com/daaku/go.zipexe v1.0.1 // indirect
	github.com/golang/protobuf v1.3.5 // indirect
	github.com/google/uuid v1.1.0
	github.com/gorilla/schema v1.1.0
)
